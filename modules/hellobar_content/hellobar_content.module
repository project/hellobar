<?php
/**
 * @file
 * Create content type for hellobar messages.
 */

include_once 'hellobar_content.features.inc';

/**
 * Implements hook_node_view().
 */
function hellobar_content_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'hello_message') {
    hellobar_initialization();
    hellobar_content_message_push($node);
  }
}

/**
 * Prepare message.
 *
 * @param object $node
 *   A $node.
 *
 * @return array
 *   Arguments for HelloBar JS.
 */
function hellobar_content_message_prepare($node) {
  $hellobar_message = $node->title;

  if ($node->field_hellobar_link) {
    $link_fields = field_get_items('node', $node, 'field_hellobar_link');
    $link_field = reset($link_fields);
    $link_field['attributes']['class'] = 'button';
    $link_element = field_view_value('node', $node, 'field_hellobar_link', $link_field);

    $hellobar_message .= ' ' . render($link_element);
  }

  $message = array(
    'message' => $hellobar_message,
    'options' => array(),
    'nid' => $node->nid,
  );

  drupal_alter('hellobar_content_message', $message, $node);

  return $message;
}

/**
 * Push Message.
 *
 * @param object $node
 *   A node.
 */
function hellobar_content_message_push($node) {
  $message = hellobar_content_message_prepare($node);
  hellobar_say(array($message));
}

/**
 * Push Multiple Message.
 *
 * @param array $nodes
 *   An associative array containing:
 *   - element: A $node object.
 */
function hellobar_content_message_push_multiple($nodes) {
  $messages = array();

  foreach ($nodes as $node) {
    $messages[] = hellobar_content_message_prepare($node);
  }

  hellobar_say($messages);
}
