<?php
/**
 * @file
 * Tests for HelloBar Preset module.
 */

class HelloBarPresetTestCase extends DrupalWebTestCase {
  /**
   * Set up.
   */
  public function setUp() {
    parent::setUp('hellobar_preset');
  }
}

class HelloBarPresetBasicTestCase extends HelloBarPresetTestCase {
  /**
   * Get info.
   *
   * @return array
   *   Test information.
   */
  public static function getInfo() {
    return array(
      'name' => 'HelloBar Preset Test',
      'description' => t('Try to Create/Load/Save/Delete presets.'),
      'group' => 'HelloBar',
    );
  }

  protected $basicFormValues = array(
    'name' => 'basic_test',
    'title' => 'basic_test',
    'type' => 'basic',
    'positioning' => 'sticky',
    'tabSide' => 'right',
    'barColor' => '#eb583c',
    'textColor' => '#ffffff',
    'linkColor' => '#80ccfb',
    'borderColor' => '#ffffff',
    'borderSize' => 3,
    'helloBarLogo' => 0,
  );

  protected $moreFormValues = array(
    'name' => 'more_test',
    'title' => 'more_test',
    'type' => 'more',
    'height' => 30,
    'showWait' => 0,
    'hideAfter' => 0,
    'wiggleWait' => 15000,
    'fontSize' => '16px',
    'lineHeight' => '',
    'fontWeight' => 'bold',
    'fontStyle' => 'normal',
    'fontFamily' => 'Arial Black',
  );

  protected $advansedFormValues = array(
    'name' => 'advansed_test',
    'title' => 'advansed_test',
    'type' => 'advanced',
    'googleFont' => 'Ubuntu',
    'tabRadius' => '5px',
    'speed' => 500,
    'shadow' => TRUE,
    'forgetful' => FALSE,
    'transition' => 'bouncy',
  );

  /**
   * Presets test.
   */
  public function testPresets() {
    $presets = array(
      'basic' => $this->basicFormValues,
      'more' => $this->moreFormValues,
      'advansed' => $this->advansedFormValues,
    );

    foreach ($presets as $type => $preset) {
      // Create Presets.
      $save_preset = hellobar_preset_save($preset);
      $this->assertTrue($save_preset, t("Create %type preset", array('%type' => $type)));

      // Load Preset.
      $preset = hellobar_preset_load($preset['name'], $preset['type']);
      $this->assertTrue($preset, t("Load %type preset", array('%type' => $type)));

      // Update Preset.
      $update_preset = hellobar_preset_save($preset);
      $this->assertTrue($update_preset, t("Update %type preset", array('%type' => $type)));

      // Delete Preset.
      $delete_preset = hellobar_preset_delete($preset['pid']);
      $this->assertTrue($delete_preset, t("Delete %type preset", array('%type' => $type)));
    }
  }
}

class HelloBarExtraFieldsTestCase extends HelloBarPresetTestCase {
  /**
   * Get info.
   *
   * @return array
   *   Test information.
   */
  public static function getInfo() {
    return array(
      'name' => 'HelloBar Extra Fields Test',
      'description' => t('Try to Create/Load/Save/Delete node with extra fields.'),
      'group' => 'HelloBar',
    );
  }

  /**
   * Set up.
   */
  public function setUp() {
    parent::setUp('hellobar_preset', 'hellobar_content');
  }

  /**
   * Get default presets.
   *
   * @param array $settings
   *   Settings for presets.
   */
  protected function setDefaultPresets(&$settings) {
    $settings['hellobar_preset_basic'] = 'crikey';
    $settings['hellobar_preset_more'] = 'default';
    $settings['hellobar_preset_advanced'] = 'default';
  }

  /**
   * Check presets into node.
   *
   * @param object $node
   *   A node.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected function fieldsIsOk($node) {
    return $node->hellobar_preset_basic == 'crikey'
      && $node->hellobar_preset_more == 'default'
      && $node->hellobar_preset_advanced == 'default';
  }

  /**
   * Extra fields test.
   */
  public function testExtraFields() {
    $settings = array('type' => 'hello_message');

    // Set default presets.
    $this->setDefaultPresets($settings);

    // Create node.
    $node = $this->drupalCreateNode($settings);
    $this->assertTrue($this->fieldsIsOk($node), t("Create node with Extra Fields"));

    // Load node.
    $node = node_load($node->nid);
    $this->assertTrue($this->fieldsIsOk($node), t("Load node with Extra Fields"));

    // Update node.
    $node->hellobar_preset_basic = 'london_fog';
    node_save($node);
    $node = node_load($node->nid);
    $field_is_ok = $node->hellobar_preset_basic == 'london_fog';
    $this->assertTrue($field_is_ok, t("Save node with Extra Fields"));

    // Delete node.
    node_delete($node->nid);

    // Try to load Extra Fields for node.
    $output = db_select('hellobar_preset_reference', 'p')
      ->fields("p", array())
      ->condition('p.nid', $node->nid)
      ->execute()->fetchAssoc();
    $this->assertFalse($output, t("Delete node with Extra Fields"));
  }
}
